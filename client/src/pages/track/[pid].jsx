import Link from "next/link";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Heading, Box, Image, Button, Text, chakra } from "@chakra-ui/react";

// Components
import Layout from "../../components/Layout";
import Thermometer from "../../components/Thermometer";

// Services
import {
  requestTrackData,
  requestAudioFeaturesData,
} from "../../services/Spotify";

function Track() {
  const router = useRouter();
  const { pid } = router.query;
  const [track, setTrack] = useState({});
  const [features, setFeatures] = useState([]);

  // Request track data
  const getTrackData = async () => {
    const trackData = await requestTrackData(pid);
    // Parse duration
    const minutes = Math.floor(trackData.duration_ms / 1000 / 60);
    const seconds = Math.floor(
      Math.floor(trackData.duration_ms / 1000) - minutes * 60
    ).toString();
    const trackLength = `${minutes}:${
      seconds.length === 1 ? "0" + seconds : seconds
    }`;
    const finalTrackData = { trackLength: trackLength, ...trackData };

    setTrack(finalTrackData);
  };
  useEffect(() => {
    getTrackData();
  }, []);

  // Request track audio features
  const getAudioFeatures = async () => {
    const featuresData = await requestAudioFeaturesData(pid);
    setFeatures(featuresData);
  };
  useEffect(() => {
    getAudioFeatures();
  }, []);

  return (
    <>
      <Layout>
        {Object.keys(track).length > 0 && (
          <Box>
            <Heading as="h1" my={4} color="teal.400">
              {track.name}
            </Heading>
            <Text fontSize="xl" my={4}>
              Track on
              <Link href={`/album/${track.album.id}`} passHref>
                <chakra.a color="teal.500">{` ${track.album.name}`}</chakra.a>
              </Link>
            </Text>
            <Text fontSize="xl" my={4}>
              Artist:
              <Link href={`/artist/${track.album.artists[0].id}`} passHref>
                <chakra.a color="teal.500">{` ${track.album.artists[0].name}`}</chakra.a>
              </Link>
            </Text>
            <Text fontSize="xl" my={4}>
              Duration: {track.trackLength}
            </Text>
            <Button as="a" href={track.external_urls.spotify}>
              Open {track.name} on Spotify
            </Button>
          </Box>
        )}
        {Object.keys(features).length > 0 && (
          <Box>
            <Thermometer features={features} />
          </Box>
        )}
      </Layout>
    </>
  );
}

export default Track;
