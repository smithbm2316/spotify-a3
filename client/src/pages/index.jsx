import Layout from "../components/Layout";
import Search from "../components/Search";
import About from "../components/About";

export default function Home() {
  return (
    <>
      <Layout>
        <About />
        <Search />
      </Layout>
    </>
  );
}
