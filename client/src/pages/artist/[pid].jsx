import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import {
  Heading,
  Image,
  Box,
  Flex,
  Button,
  UnorderedList,
  ListItem,
} from '@chakra-ui/react';

// Components
import Layout from '../../components/Layout';
import Carousel from '../../components/Carousel';
import TrackList from '../../components/TrackList';

// Services
import {
  requestArtistData,
  requestArtistAlbumsData,
  requestTopTracksData,
  requestSimilarArtistData,
} from '../../services/Spotify';

function Artist() {
  const router = useRouter();
  const { pid } = router.query;
  const [artist, setArtist] = useState({});
  const [albums, setAlbums] = useState({});
  const [topTracks, setTopTracks] = useState({});
  const [similarArtists, setSimilarArtists] = useState({});

  // Request artist data
  const getArtistData = async () => {
    // const artistData = await requestSpotifyData(["artist", pid]);
    const artistData = await requestArtistData(pid);
    setArtist(artistData);
  };
  useEffect(() => {
    getArtistData();
  }, [pid]);

  // Request album data
  const getAlbumData = async () => {
    // const albumData = await requestArtistAlbumsData(["artist-albums", pid]);
    const albumData = await requestArtistAlbumsData(pid);
    setAlbums(albumData);
  };
  useEffect(() => {
    getAlbumData();
  }, [pid]);

  // Request top tracks data
  const getTopTracksData = async () => {
    // const topTracksData = await requestSpotifyData(["artist-top-tracks", pid]);
    const topTracksData = await requestTopTracksData(pid);
    setTopTracks(topTracksData);
  };
  useEffect(() => {
    getTopTracksData();
  }, [pid]);

  // Request similar artists data
  const getSimilarArtistsData = async () => {
    const similarArtistsData = await requestSimilarArtistData(pid);
    setSimilarArtists(similarArtistsData);
  };
  useEffect(() => {
    getSimilarArtistsData();
  }, [pid]);

  return (
    <>
      <Layout>
        {Object.keys(artist).length > 0 && Object.keys(albums).length > 0 && (
          <Box>
            <Heading as="h1" mb={6} color="teal.500">
              {artist.name}
            </Heading>
            <Image
              src={
                artist.images.length > 0 ? artist.images[0].url : '/unknown.jpg'
              }
              alt={artist.name}
              width="500px"
              height="auto"
            />
            <Button as="a" href={artist.external_urls.spotify} my={6}>
              Open {artist.name}'s profile on Spotify
            </Button>
            <Heading as="h3" fontSize="xl" color="teal.500">
              {artist.name}'s Albums
            </Heading>
            <Carousel
              flexAlign="flex-start"
              searchResults={albums}
              carouselId="artist-albums"
            />
          </Box>
        )}
        {Object.keys(artist).length > 0 &&
          Object.keys(topTracks).length > 0 &&
          Object.keys(similarArtists).length > 0 && (
            <Box>
              <Flex justify="space-around">
                <Box mr={8}>
                  <Heading as="h2" color="teal.500">
                    Genres
                  </Heading>
                  <UnorderedList>
                    {artist.genres.map((genre, index) => (
                      <ListItem key={index}>{genre}</ListItem>
                    ))}
                  </UnorderedList>
                </Box>
                <TrackList searchResults={topTracks} />
              </Flex>
              <Carousel
                flexAlign="flex-end"
                py={4}
                searchResults={similarArtists}
                carouselId="artist-related-artists"
              />
            </Box>
          )}
      </Layout>
    </>
  );
}

export default Artist;
