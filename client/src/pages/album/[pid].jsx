import Link from "next/link";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Heading, Box, Image, Button, Text, chakra } from "@chakra-ui/react";

// Components
import Layout from "../../components/Layout";
import TrackList from "../../components/TrackList";

// Services
import {
  requestAlbumData,
  requestAlbumTracksData,
} from "../../services/Spotify";

function Album() {
  const router = useRouter();
  const { pid } = router.query;
  const [album, setAlbum] = useState({});
  const [tracks, setTracks] = useState([]);

  // Request album data
  const getAlbumData = async () => {
    const albumData = await requestAlbumData(pid);
    setAlbum(albumData);
  };
  useEffect(() => {
    getAlbumData();
  }, []);

  // Request album tracks data
  const getAlbumTracksData = async () => {
    const tracksData = await requestAlbumTracksData(pid);
    setTracks(tracksData);
  };
  useEffect(() => {
    getAlbumTracksData();
  }, []);

  return (
    <>
      <Layout>
        {Object.keys(album).length > 0 && (
          <Box>
            <Heading as="h1" my={4} color="teal.500">
              {album.name}
            </Heading>
            <Image
              src={
                album.images.length > 0 ? album.images[0].url : "/unknown.jpg"
              }
              alt={album.name}
              width="500px"
              height="auto"
            />
            <Text fontSize="lg" my={4}>
              Artist:
              <Link href={`/artist/${album.artists[0].id}`} passHref>
                <chakra.a color="orange.500">{` ${album.artists[0].name}`}</chakra.a>
              </Link>
            </Text>
            <Button as="a" href={album.external_urls.spotify}>
              Open {album.name} on Spotify
            </Button>
          </Box>
        )}
        {Object.keys(tracks).length > 0 && (
          <TrackList searchResults={tracks} forPage="album" />
        )}
      </Layout>
    </>
  );
}

export default Album;
