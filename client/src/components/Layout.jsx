import Head from "next/head";
import { Center, SimpleGrid } from "@chakra-ui/react";
import Navbar from "./Navbar";

function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Spotify Browser</title>
      </Head>
      <Navbar />
      <Center as="main">
        <SimpleGrid
          py={8}
          width="80%"
          columns={["auto", "auto", "auto", 2]}
          rows={[2, 2, 2, "auto"]}
          spacing={10}
        >
          {children}
        </SimpleGrid>
      </Center>
    </>
  );
}

export default Layout;
