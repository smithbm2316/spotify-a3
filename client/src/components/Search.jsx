import React, { useState } from "react";
import { Box, Heading, Input, Select, Button } from "@chakra-ui/react";
import Carousel from "./Carousel";
import TrackList from "./TrackList";

function Search() {
  const baseURI = "http://localhost:8888/search";
  const [searchResults, setSearchResults] = useState({});
  const [searchType, setSearchType] = useState("");

  async function onSearch(event) {
    // Get reference to the current text the user is searching and whether they are searching
    // for artist, album, or track
    const searchQuery = document.querySelector("input").value;
    const selectValue = document.querySelector("select").value;
    setSearchType(selectValue);

    // If we have a valid query then make a request
    if (selectValue !== "" && searchQuery !== "") {
      // Request the data
      const response = await fetch(`${baseURI}/${selectValue}/${searchQuery}`);
      const results = await response.json();

      // Parse the data into a new object format with only the info we need
      const parsedResults = results[`${selectValue}s`].items.map(
        (result, index) => {
          // Parse link properly
          const parsedLink = `/${selectValue}/${result.id}`;

          // Parse data for tracks
          if (selectValue === "track") {
            // Change duration from milliseconds into minutes and seconds
            const trackMinutes = Math.floor(result.duration_ms / 1000 / 60);
            const trackSeconds = Math.floor(
              Math.floor(result.duration_ms / 1000) - trackMinutes * 60
            ).toString();
            const trackLength = `${trackMinutes}:${
              trackSeconds.length === 1 ? "0" + trackSeconds : trackSeconds
            }`;

            return {
              name: result.name,
              artist: result.artists[0].name,
              link: parsedLink,
              id: result.id,
              number: index + 1,
              duration: trackLength,
              album: result.album.name,
            };
          }
          // Parse data for albums/artists, since the structure is the same
          else {
            return {
              name: result.name,
              imageURL:
                result.images.length > 0
                  ? result.images[0].url
                  : "/unknown.jpg",
              link: parsedLink,
              id: result.id,
            };
          }
        }
      );
      // Update state with correctly parsed search results
      setSearchResults(parsedResults);
    }
  }

  return (
    <Box>
      <Heading as="h1" color="teal.500">
        Search Spotify
      </Heading>
      <Input
        placeholder="Enter a track, artist, or album name"
        variant="flushed"
        my={4}
      />
      <Select my={4} variant="filled" placeholder="Track, Artist, or Album?">
        <option value="album">Album</option>
        <option value="artist">Artist</option>
        <option value="track">Track</option>
      </Select>
      <Button onClick={onSearch} my={4}>
        Search
      </Button>
      {/* Only display Carousel and TrackList components if the user has made a search */}
      {Object.entries(searchResults).length > 0 &&
        (searchType === "artist" || searchType === "album") && (
          <Carousel
            flexAlign="center"
            searchResults={searchResults}
            carouselId="artist-results"
          />
        )}
      {Object.entries(searchResults).length > 0 && searchType === "track" && (
        <TrackList searchResults={searchResults} forPage="search" />
      )}
    </Box>
  );
}

export default Search;
