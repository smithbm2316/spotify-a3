import React, { useState } from "react";
import {
  Box,
  Heading,
  Button,
  Image,
  useColorModeValue,
} from "@chakra-ui/react";

function About() {
  const [username, setUsername] = useState("???");
  const [profilePic, setProfilePic] = useState("/unknown.jpg");
  const [profileLink, setProfileLink] = useState("#");

  const headingColor = useColorModeValue("gray.700", "gray.100");

  async function getUserInfo() {
    if (username === "???") {
      const response = await fetch("http://localhost:8888/me");
      const userData = await response.json();
      setUsername(userData.display_name);
      setProfilePic(userData.images[0].url);
      setProfileLink(userData.external_urls.spotify);
    }
  }

  return (
    <Box>
      <Heading as="h2" size="lg" color={headingColor}>
        Logged in user: {username}
      </Heading>
      <Button
        onClick={getUserInfo}
        size="sm"
        colorScheme="teal"
        variant="outline"
        my={4}
      >
        Load info about me
      </Button>
      <Image
        src={profilePic}
        alt={username === "???" ? "Unknown user" : username}
        boxSize="300px"
        objectFit="contain"
      />
      <Button
        as="a"
        size="sm"
        colorScheme="teal"
        variant="outline"
        my={4}
        href={profileLink}
      >
        Open profile on Spotify
      </Button>
    </Box>
  );
}

export default About;
