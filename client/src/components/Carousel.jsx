import { useEffect } from "react";
import { Flex, IconButton, chakra } from "@chakra-ui/react";
import { ArrowBackIcon, ArrowForwardIcon } from "@chakra-ui/icons";
import CarouselCard from "./CarouselCard";

const CarouselWrapper = chakra(Flex, {
  baseStyle: {
    overflowX: "hidden",
    scrollSnapType: "x mandatory",
    width: "400px",
    height: "400px",
  },
});

function Carousel({ searchResults, carouselId, flexAlign }) {
  // Reset the carousel to the first item in the list of images whenever we load in new search data
  useEffect(() => {
    const carouselWrapper = document.querySelector(`.${carouselId}`);
    carouselWrapper.scrollTo({ left: 0, top: 0, behavior: "smooth" });
  }, [searchResults]);

  // Change the to the previous or next slide using scrollSnap and scrollBy()
  function changeSlide(direction) {
    const carouselWrapper = document.querySelector(`.${carouselId}`);
    const carouselCardWidth = carouselWrapper.querySelector("a").clientWidth;
    const moveBy =
      direction === "prev" ? -carouselCardWidth : carouselCardWidth;
    carouselWrapper.scrollBy({ left: moveBy, top: 0, behavior: "smooth" });
  }

  return (
    <Flex alignItems="center" justifyContent={flexAlign} width="100%" my={8}>
      <IconButton
        aria-label="Previous search result"
        colorScheme="teal"
        icon={<ArrowBackIcon />}
        onClick={() => changeSlide("prev")}
        variant="outline"
        mx={4}
      />
      <CarouselWrapper className={carouselId}>
        {searchResults.map((result) => {
          return (
            <CarouselCard
              name={result.name}
              link={result.link}
              imageURL={result.imageURL}
              key={result.id}
            />
          );
        })}
      </CarouselWrapper>
      <IconButton
        aria-label="Next search result"
        colorScheme="teal"
        icon={<ArrowForwardIcon />}
        onClick={() => changeSlide("next")}
        variant="outline"
        mx={4}
      />
    </Flex>
  );
}

export default Carousel;
