import { chakra } from "@chakra-ui/react";
import Link from "next/link";

// Set up table components with proper styling

const Table = chakra("table", {
  baseStyle: {
    borderCollapse: "collapse",
  },
});

const TableHead = chakra("thead", {
  baseStyle: {
    bg: "teal.700",
    color: "gray.50",
  },
});

const TableBody = chakra("tbody", {
  baseStyle: {
    bg: "gray.200",
    color: "gray.900",
  },
});

const TableRow = chakra("tr", {
  baseStyle: {
    borderColor: "gray.300",
    borderWidth: "1px",
    borderStyle: "solid",
  },
});

const TableCell = chakra("td", {
  baseStyle: {
    padding: "0.4rem",
  },
});

function TrackList({ searchResults, forPage }) {
  return (
    <Table>
      <TableHead>
        <TableRow border="none">
          <TableCell fontWeight="bold">#</TableCell>
          <TableCell fontWeight="bold">Track</TableCell>
          <TableCell fontWeight="bold">Duration</TableCell>
          {forPage === "search" && (
            <TableCell fontWeight="bold">Artist</TableCell>
          )}
          {forPage === "search" && (
            <TableCell fontWeight="bold">Album</TableCell>
          )}
        </TableRow>
      </TableHead>
      <TableBody>
        {searchResults.map((result) => {
          return (
            <TableRow key={result.id}>
              <TableCell>{result.number}</TableCell>
              <TableCell>
                <Link href={result.link} passHref>
                  <chakra.a
                    color="teal.600"
                    fontWeight="bold"
                    _hover={{ cursor: "pointer", textDecoration: "underline" }}
                  >
                    {result.name}
                  </chakra.a>
                </Link>
              </TableCell>
              <TableCell>{result.duration}</TableCell>
              {forPage === "search" && <TableCell>{result.artist}</TableCell>}
              {forPage === "search" && (
                <TableCell fontWeight="italic">{result.album}</TableCell>
              )}
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  );
}

export default TrackList;
