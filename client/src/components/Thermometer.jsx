import { Progress, Box, Text } from "@chakra-ui/react";
import { jsx } from "@emotion/react";
import chroma from "chroma-js";

function Thermometer({ features }) {
  return (
    <>
      {features.map(([feature, value], index) => {
        if (feature === "tempo") {
          const editedValue = Math.floor(((240 - value) / 240) * 100);
          const rgbColors = chroma.mix("red", "blue", editedValue / 100, "rgb")[
            "_rgb"
          ];
          const blendedColor = `rgb(${rgbColors[0]}, ${rgbColors[1]}, ${rgbColors[2]})`;
          return (
            <Box my={2} key={index}>
              <Text
                textAlign="right"
                color="blue.500"
              >{`${feature}: ${Math.floor(value)} / 240 bpm`}</Text>
              <Progress
                css={{ backgroundColor: blendedColor }}
                my={2}
                colorScheme="gray"
                value={editedValue}
              />
            </Box>
          );
        } else {
          const stringValue = (value * 100).toString();
          const period = stringValue.indexOf(".");
          const editedValue = stringValue.substring(0, period + 3);

          // chroma-js
          const rgbColors = chroma.mix("red", "blue", 1 - value, "rgb")["_rgb"];
          const blendedColor = `rgb(${rgbColors[0]}, ${rgbColors[1]}, ${rgbColors[2]})`;

          return (
            <Box my={2} key={index}>
              <Text
                textAlign="right"
                color="blue.500"
              >{`${feature}: ${editedValue}%`}</Text>
              <Progress
                css={{ backgroundColor: blendedColor }}
                my={2}
                colorScheme="gray"
                value={100 - value * 100}
              />
            </Box>
          );
        }
      })}
    </>
  );
}

export default Thermometer;
