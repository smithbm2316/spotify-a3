import { Image, Box, chakra } from "@chakra-ui/react";
import Link from "next/link";

const CarouselItem = chakra(Link, {
  baseStyle: {
    scrollSnapAlign: "start",
    display: "block",
  },
});

const Figure = chakra("figure", {
  baseStyle: {
    pos: "relative",
    scrollSnapAlign: "start",
    width: "400px",
    height: "400px",
  },
});

function CarouselCard({ name, link, imageURL }) {
  return (
    <CarouselItem href={link}>
      <a>
        <Figure>
          <Image src={imageURL} alt={name} width="100%" height="100%" />
          <Box
            as="figcaption"
            pos="absolute"
            bottom="0"
            width="100%"
            p={2}
            bg="rgba(0, 0, 0, 0.4)"
            color="white"
          >
            {name}
          </Box>
        </Figure>
      </a>
    </CarouselItem>
  );
}
export default CarouselCard;
