import { Link as NextLink } from "next/link";
import {
  Link,
  Center,
  Flex,
  Spacer,
  useColorModeValue,
} from "@chakra-ui/react";
import ThemeToggle from "./ThemeToggle";

function Navbar() {
  const linkColor = useColorModeValue("gray.900", "gray.100");
  const bgColor = useColorModeValue("gray.200", "teal.800");

  return (
    <Center as="nav" bg={bgColor}>
      <Flex py={4} align="center" width="80%">
        <Link
          as={NextLink}
          color={linkColor}
          fontWeight="bold"
          fontSize="2xl"
          href="/"
        >
          Browsing Spotify
        </Link>
        <Spacer />
        <Link color={linkColor} href="http://localhost:8888/login">
          Log in
        </Link>
        <ThemeToggle />
      </Flex>
    </Center>
  );
}

export default Navbar;
