const baseURI = "http://localhost:8888";

async function requestSpotifyData(params) {
  let url = `${baseURI}`;
  params.forEach((param) => {
    url += `/${param}`;
  });

  try {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (error) {
    alert(error.message);
  }
}

export async function requestArtistData(id) {
  const response = await requestSpotifyData(["artist", id]);
  return response;
}

export async function requestArtistAlbumsData(id) {
  const albums = await requestSpotifyData(["artist-albums", id]);
  return albums.items.map((album) => {
    const albumLink = `/album/${album.id}`;
    return {
      name: album.name,
      imageURL: album.images.length > 0 ? album.images[0].url : "/unknown.jpg",
      link: albumLink,
      id: album.id,
    };
  });
}

export async function requestTopTracksData(id) {
  const data = await requestSpotifyData(["artist-top-tracks", id]);

  return data.tracks.map((track, index) => {
    // Change duration from milliseconds into minutes and seconds
    const trackMinutes = Math.floor(track.duration_ms / 1000 / 60);
    const trackSeconds = Math.floor(
      Math.floor(track.duration_ms / 1000) - trackMinutes * 60
    ).toString();
    const trackLength = `${trackMinutes}:${
      trackSeconds.length === 1 ? "0" + trackSeconds : trackSeconds
    }`;

    // Parse track link
    const trackLink = `/track/${track.id}`;

    return {
      name: track.name,
      artist: track.artists[0].name,
      link: trackLink,
      id: track.id,
      number: index + 1,
      duration: trackLength,
      album: track.album.name,
    };
  });
}

export async function requestSimilarArtistData(id) {
  const data = await requestSpotifyData(["artist-related-artists", id]);
  return data.artists.map((artist) => {
    const parsedLink = `/artist/${artist.id}`;

    return {
      name: artist.name,
      link: parsedLink,
      imageURL:
        artist.images.length > 0 ? artist.images[0].url : "/unknown.jpg",
      id: artist.id,
    };
  });
}

export async function requestAlbumData(id) {
  const data = await requestSpotifyData(["album", id]);
  return data;
}

export async function requestAlbumTracksData(id) {
  const data = await requestSpotifyData(["album-tracks", id]);

  return data.items.map((track) => {
    // Change duration from milliseconds into minutes and seconds
    const trackMinutes = Math.floor(track.duration_ms / 1000 / 60);
    const trackSeconds = Math.floor(
      Math.floor(track.duration_ms / 1000) - trackMinutes * 60
    ).toString();
    const trackLength = `${trackMinutes}:${
      trackSeconds.length === 1 ? "0" + trackSeconds : trackSeconds
    }`;

    // Parse track link
    const trackLink = `/track/${track.id}`;

    return {
      name: track.name,
      link: trackLink,
      id: track.id,
      number: track.track_number,
      duration: trackLength,
    };
  });
}

export async function requestTrackData(id) {
  const data = await requestSpotifyData(["track", id]);
  return data;
}

export async function requestAudioFeaturesData(id) {
  const data = await requestSpotifyData(["track-audio-features", id]);
  const unwanted = [
    "key",
    "loudness",
    "mode",
    "type",
    "uri",
    "track_href",
    "analysis_url",
    "duration_ms",
    "time_signature",
    "id",
  ];

  const editedData = Object.entries(data).filter(([feature, value]) => {
    if (!unwanted.includes(feature)) {
      return [feature, value];
    }
  });
  return editedData;
}
